package com.ingenio.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
public class EprashasanNotificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EprashasanNotificationServiceApplication.class, args);
	}

}
