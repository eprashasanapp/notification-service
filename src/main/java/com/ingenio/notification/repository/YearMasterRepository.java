package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SemesterMasterModel;
import com.ingenio.notification.model.YearMasterModel;

public interface YearMasterRepository extends JpaRepository<YearMasterModel, Integer>{

	@Query(value="select a from YearMasterModel a where a.schoolMasterModel.schoolid=:defaultSchoolId and a.isDel='0'")
	List<YearMasterModel> getYearByDefaultSchool(Integer defaultSchoolId);

	@Query(value="SELECT COALESCE(MAX(a.yearId),0)+1 from YearMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	String getMaxYearId(Integer schoolId);

	@Query(value="select a from YearMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.year=:year and a.isDel='0'")
	YearMasterModel getAlreadyExistYear(String year, Integer schoolId);

}
