package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SubjectWithPatternModel;

public interface SubjectWithPatternRepository extends JpaRepository<SubjectWithPatternModel, Integer>{

	@Query("select a from SubjectWithPatternModel a where a.schoolMasterModel.schoolid=:defaultSchoolId")
	List<SubjectWithPatternModel> getList(Integer defaultSchoolId);

	@Query(value="SELECT COALESCE(MAX(a.subjectPatternId),0)+1 from SubjectWithPatternModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	String getMaxId(Integer schoolId);

	@Query("select a from SubjectWithPatternModel a where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and a.subjectModel.subjectId=:subjectId and a.subjectPatternModel.patternId=:patternId")
	List<SubjectWithPatternModel> findData(Integer schoolid, int subjectId, int patternId);

	@Query("select a from SubjectWithPatternModel a where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' and a.subjectModel.subjectId=:subjectId")
	List<SubjectWithPatternModel> findDataBySubject(Integer schoolid, int subjectId);

}
