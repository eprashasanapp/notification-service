package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SubjectMasterTypeMasterModel;

public interface SubjectTypeMasterRepository extends JpaRepository<SubjectMasterTypeMasterModel, Integer>{

	@Query("select a from SubjectMasterTypeMasterModel a where a.schoolMaster.schoolid=:defaultSchoolId and a.isDel='0'")
	List<SubjectMasterTypeMasterModel> getSubjectTypeList(Integer defaultSchoolId);

	@Query("select a from SubjectMasterTypeMasterModel a where a.schoolMaster.schoolid=:schoolid and a.isDel='0' and a.subjectTypeName=:subjectTypeName")
	List<SubjectMasterTypeMasterModel> checkAlreadyExist(String subjectTypeName, Integer schoolid);
	
	@Query(value="SELECT COALESCE(MAX(a.subjectTypeId),0)+1 from SubjectMasterTypeMasterModel a where a.schoolMaster.schoolid=:schoolId and a.isDel='0' ")
	String getMaxId(Integer schoolId);

	@Query("select a from SubjectMasterTypeMasterModel a where a.schoolMaster.schoolid=:schoolid and a.isDel='0' and a.subjectTypeName=:subjectTypeName")
	SubjectMasterTypeMasterModel getSubjectMasterDetails(Integer schoolid, String subjectTypeName);

}
