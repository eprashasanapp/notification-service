package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SubjectTypeModel;

public interface SubjectTypeRepository extends JpaRepository<SubjectTypeModel, Integer>{

	@Query("select a from SubjectTypeModel a where a.schoolMasterModel.schoolid=:defaultSchoolId")
	List<SubjectTypeModel> getSubjectTypeList(Integer defaultSchoolId);

	@Query("select a from SubjectTypeModel a where a.schoolMasterModel.schoolid=:schoolid and a.subjectTypeName=:subjectTypeName")
	List<SubjectTypeModel> findData(Integer schoolid, String subjectTypeName);

	@Query(value="SELECT COALESCE(MAX(a.subjectTypeId),0)+1 from SubjectTypeModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	String getMaxId(Integer schoolId);

	@Query("select a from SubjectTypeModel a where a.schoolMasterModel.schoolid=:schoolid and a.subjectTypeName=:subjectTypeName")
	List<SubjectTypeModel> findDataNew(Integer schoolid, String subjectTypeName);

}
