package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SyllabusChapterMasterModel;

public interface SyllabusChapterMasterRepository extends JpaRepository<SyllabusChapterMasterModel, Integer>{

	@Query(value="select a from SyllabusChapterMasterModel a where a.syllChapterName=:chapterName and a.subject.subjectId=:subjectId and a.schoolId.schoolid=:schoolId")
	List<SyllabusChapterMasterModel> CheckAlreadyExist(String chapterName, int subjectId, Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.syllChapterId),0)+1 from SyllabusChapterMasterModel a where a.schoolId.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
