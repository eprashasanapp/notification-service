package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SubjectWithTypeModel;

public interface SubjectWithTypeRepository extends JpaRepository<SubjectWithTypeModel, Integer>{

	@Query("select a from SubjectWithTypeModel a where a.subjectModel.subjectId=:subjectId")
	List<SubjectWithTypeModel> findData(int subjectId);

//	@Query("select a from SubjectWithTypeModel a where a.subjectModel.subjectId=:subjectId and a.subjectTypeId=:subjectTypeId")
//	List<SubjectWithTypeModel> checkAlreadyExist(int subjectId, Integer subjectTypeId);

}
