package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.StandardMasterModel;
import com.ingenio.notification.model.YearMasterModel;

public interface StandardMasterRepository extends JpaRepository<StandardMasterModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.standardId),0)+1 from StandardMasterModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	@Query(value="select a from StandardMasterModel a where a.schoolMasterModel.schoolid=:defaultSchoolId and a.isDel='0'")
	List<StandardMasterModel> getStandardByDefaultSchool(Integer defaultSchoolId);

	@Query(value="select a from StandardMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.standardName=:standardName and a.isDel='0'")
	StandardMasterModel getAlreadyExistStandard(String standardName, Integer schoolId);

	
}
