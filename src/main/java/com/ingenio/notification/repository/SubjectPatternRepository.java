package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SubjectPatternModel;

public interface SubjectPatternRepository extends JpaRepository<SubjectPatternModel, Integer>{

	@Query("select a from SubjectPatternModel a where a.schoolMaster.schoolid=:defaultSchoolId and a.isDel='0'")
	List<SubjectPatternModel> getPatternsList(Integer defaultSchoolId);

	@Query("select a from SubjectPatternModel a where a.schoolMaster.schoolid=:schoolid and a.isDel='0' and a.patternName=:patternName and a.patternType=:patternType")
	List<SubjectPatternModel> CheckAlreadyExist(String patternName, Integer schoolid, int patternType);
	
	@Query(value="SELECT COALESCE(MAX(a.patternId),0)+1 from SubjectPatternModel a where a.schoolMaster.schoolid=:schoolId and a.isDel='0' ")
	String getMaxId(Integer schoolId);

	@Query("select a from SubjectPatternModel a where a.schoolMaster.schoolid=:schoolid and a.isDel='0' and a.patternName=:patternName and a.patternType=:patternType")
	SubjectPatternModel getSubjectPattern(Integer schoolid, String patternName,int patternType);


}
