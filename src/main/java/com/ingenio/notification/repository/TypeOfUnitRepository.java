package com.ingenio.notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.TypeOfUnitModel;

public interface TypeOfUnitRepository extends JpaRepository<TypeOfUnitModel, Integer> {

	@Query(value="SELECT COALESCE(MAX(a.typeUnitId),0)+1 from TypeOfUnitModel a where a.schoolMasterModel.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

	
}
