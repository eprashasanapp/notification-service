package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.GlobalStandardMasterModel;

public interface GlobalStandardMasterRepository extends JpaRepository<GlobalStandardMasterModel, Integer> {

	

}
