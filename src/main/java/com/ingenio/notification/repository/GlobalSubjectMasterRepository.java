package com.ingenio.notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.notification.model.GlobalSubjectMasterModel;

public interface GlobalSubjectMasterRepository extends JpaRepository<GlobalSubjectMasterModel, Integer>{

}
