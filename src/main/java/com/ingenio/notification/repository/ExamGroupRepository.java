package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.ExamGroupModel;
import com.ingenio.notification.model.ExamSubGroupModel;
import com.ingenio.notification.model.SemesterMasterModel;
import com.ingenio.notification.model.StandardMasterModel;
import com.ingenio.notification.model.YearMasterModel;

public interface ExamGroupRepository extends JpaRepository<ExamGroupModel, Integer> {

	@Query(value="select a from ExamGroupModel a where a.schoolMasterModel.schoolid=:demoSchoolId and a.isDel='0' ")
	List<ExamGroupModel> getModelBySchoolId(int demoSchoolId);

	@Query(value="select a from YearMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.year=:year and a.isDel='0' ")
	YearMasterModel getYearMasterModel(String year, Integer schoolId);

	@Query(value="select a from StandardMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.standardName=:standardName and a.isDel='0'")
	StandardMasterModel getStandardMasterModel(String standardName, Integer schoolId);

	@Query(value="select a from SemesterMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.semesterName=:semesterName and a.isDel='0'")
	SemesterMasterModel getSemesterMasterModel(String semesterName, Integer schoolId);

	@Query(value="SELECT COALESCE(MAX(a.groupId),0)+1 from ExamGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'")
	String getMaxGroupId(Integer schoolId);

	@Query(value="select a from ExamSubGroupModel a where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0' ")
	List<ExamSubGroupModel> getSubGroupModelBySchoolId(Integer schoolid);

	@Query(value="select a from ExamGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.groupName=:groupName and a.isDel='0'")
	ExamGroupModel getGroupModel(String groupName, Integer schoolId);

	@Query(value="select a from ExamGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.groupName=:groupName and a.yearMasterModel.yearId=:yearId and a.standardMasterModel.standardId=:standardId and a.semesterMasterModel.semesterId=:semesterId and a.isDel='0'")
	List<ExamGroupModel> getAlreadyExistGroup(Integer yearId, Integer standardId, int semesterId, String groupName,
			Integer schoolId);

	

	
}
