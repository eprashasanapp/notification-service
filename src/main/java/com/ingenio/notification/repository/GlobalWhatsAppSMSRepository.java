package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ingenio.notification.model.GlobalWhatsAppSMSModel;
import com.ingenio.notification.model.SchoolWiseWhatsAppSMSModel;

public interface GlobalWhatsAppSMSRepository extends JpaRepository<GlobalWhatsAppSMSModel, Integer>{

	@Query("select a from SchoolWiseWhatsAppSMSModel a where a.schoolMasterModel.schoolid=:schoolId ")
	List<SchoolWiseWhatsAppSMSModel> getSchoolWiseSetting(Integer schoolId);

	@Transactional
	@Modifying
	@Query("update SchoolWiseWhatsAppSMSModel a Set a.assignMsgCount =:totalCount  where  a.schoolMasterModel.schoolid=:schoolId  ")
	void updateSchoolAssignMessage(Integer schoolId, String totalCount);

}
