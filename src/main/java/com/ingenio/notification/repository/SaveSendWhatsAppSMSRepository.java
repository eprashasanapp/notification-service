package com.ingenio.notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingenio.notification.model.SaveSendWhatsAppSMSModel;

public interface SaveSendWhatsAppSMSRepository extends JpaRepository<SaveSendWhatsAppSMSModel, Integer>{

}
