package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.ExamSubToStdMapping;

public interface ExamSubToStdMappingRepository extends JpaRepository<ExamSubToStdMapping, Integer> {

	@Query("select a from ExamSubToStdMapping a where a.subjectMaster.subjectId=:subjectId and a.standardmaster.standardId=:standardId and a.schoolMaster.schoolid=:schoolid")
	List<ExamSubToStdMapping> CheckAlreadyExist(Integer standardId, int subjectId, Integer schoolid);

	@Query(value="SELECT COALESCE(MAX(a.subToStdMappingId),0)+1 from ExamSubToStdMapping a where a.schoolMaster.schoolid=:schoolId ")
	String getMaxId(Integer schoolId);

}
