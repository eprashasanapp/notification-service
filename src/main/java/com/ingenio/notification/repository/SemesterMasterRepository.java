package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.SemesterMasterModel;

public interface SemesterMasterRepository extends JpaRepository<SemesterMasterModel, Integer>{

	@Query(value="select a from SemesterMasterModel a where a.schoolMasterModel.schoolid=:schoolid and a.isDel='0'")
	List<SemesterMasterModel> getSemesterByGroupId(Integer schoolid);

	@Query(value="SELECT COALESCE(MAX(a.semesterId),0)+1 from SemesterMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	String getMaxSemesterId(Integer schoolId);

	@Query(value="select a from SemesterMasterModel a where a.schoolMasterModel.schoolid=:schoolId and a.semesterName=:semesterName and a.isDel='0' ")
	SemesterMasterModel getAlreadyExistSemester(String semesterName, Integer schoolId);

}
