package com.ingenio.notification.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenio.notification.model.SchoolMasterModel;

@Repository
public interface SchoolMasterRepository extends JpaRepository<SchoolMasterModel, Integer>{
	
	SchoolMasterModel findBySchoolid(Integer schoolid);

}
