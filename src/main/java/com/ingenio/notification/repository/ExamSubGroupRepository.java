package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.ExamSubGroupModel;

public interface ExamSubGroupRepository extends JpaRepository<ExamSubGroupModel, Integer>{

	@Query(value="SELECT COALESCE(MAX(a.subGroupId),0)+1 from ExamSubGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0' ")
	String getMaxSubGroupId(Integer schoolId);

	@Query(value="select a from ExamSubGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.subGroupName=:subGroupName and a.isDel='0'")
	List<ExamSubGroupModel> getSubGroupModel(String subGroupName, Integer schoolId);

	@Query(value="select a from ExamSubGroupModel a where a.schoolMasterModel.schoolid=:schoolId and a.subGroupName=:subGroupName and a.examGroupModel.groupId=:groupId and a.isDel='0'")
	List<ExamSubGroupModel> getAlreadyExist(int groupId, String subGroupName, Integer schoolId);

}
