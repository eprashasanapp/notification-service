package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.StandardMasterModel;
import com.ingenio.notification.model.SubjectModel;
import com.ingenio.notification.model.YearMasterModel;

public interface SubjectRepository extends JpaRepository<SubjectModel, Integer>{

	@Query(value="select a from SubjectModel a where a.schoolMasterModel.schoolid=:defaultSchoolId and a.isDel='0' ")
	List<SubjectModel> getSubjectListByDefaultSchoolId(Integer defaultSchoolId);

	@Query(value="SELECT COALESCE(MAX(a.subjectId),0)+1 from SubjectModel a where a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'")
	String getMaxSubjectId(Integer schoolId);

	@Query(value="select a from StandardMasterModel a where a.standardName=:standardName and a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'")
	StandardMasterModel getStandardModel(String standardName, Integer schoolId);

	@Query(value="select a from YearMasterModel a where a.year=:year and a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'")
	YearMasterModel getYearMasterModel(String year, Integer schoolId);

	@Query(value="select a from SubjectModel a where a.standardMasterModel.standardId=:standardId and a.yearMasterModel.yearId=:yearId and a.schoolMasterModel.schoolid=:schoolId and a.subjectName=:subjectName")
	List<SubjectModel> CheckAlreadyExist(Integer standardId, Integer yearId,Integer schoolId, String subjectName);

	@Query(value="select a from SubjectModel a where a.subjectName=:subjectName and a.schoolMasterModel.schoolid=:schoolId and a.isDel='0'")
	SubjectModel getSubjectList(String subjectName, Integer schoolId);

}
