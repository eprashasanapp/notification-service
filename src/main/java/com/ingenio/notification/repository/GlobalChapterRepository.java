package com.ingenio.notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ingenio.notification.model.GlobalChapterModel;

public interface GlobalChapterRepository extends JpaRepository<GlobalChapterModel, Integer>{

	@Query(value="select a from GlobalChapterModel a where a.globalSubjectMasterModel.globalSubjectId=:globalSubjectId")
	List<GlobalChapterModel> findByGlobalSubject(Integer globalSubjectId);

}
