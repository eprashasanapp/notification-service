package com.ingenio.notification.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ingenio.notification.model.AppUserRoleModel;
import com.ingenio.notification.model.ExamGroupModel;
import com.ingenio.notification.model.ExamSubGroupModel;
import com.ingenio.notification.model.ExamSubToStdMapping;
import com.ingenio.notification.model.GlobalChapterModel;
import com.ingenio.notification.model.GlobalStandardMasterModel;
import com.ingenio.notification.model.SchoolMasterModel;
import com.ingenio.notification.model.SemesterMasterModel;
import com.ingenio.notification.model.StandardMasterModel;
import com.ingenio.notification.model.SubjectMasterTypeMasterModel;
import com.ingenio.notification.model.SubjectModel;
import com.ingenio.notification.model.SubjectPatternModel;
import com.ingenio.notification.model.SubjectTypeModel;
import com.ingenio.notification.model.SubjectWithPatternModel;
import com.ingenio.notification.model.SubjectWithTypeModel;
import com.ingenio.notification.model.SyllabusChapterMasterModel;
import com.ingenio.notification.model.TypeOfUnitModel;
import com.ingenio.notification.model.YearMasterModel;
import com.ingenio.notification.repository.ExamGroupRepository;
import com.ingenio.notification.repository.ExamSubGroupRepository;
import com.ingenio.notification.repository.ExamSubToStdMappingRepository;
import com.ingenio.notification.repository.GlobalChapterRepository;
import com.ingenio.notification.repository.GlobalStandardMasterRepository;
import com.ingenio.notification.repository.GlobalSubjectMasterRepository;
import com.ingenio.notification.repository.SchoolMasterRepository;
import com.ingenio.notification.repository.SemesterMasterRepository;
import com.ingenio.notification.repository.StandardMasterRepository;
import com.ingenio.notification.repository.SubjectPatternRepository;
import com.ingenio.notification.repository.SubjectRepository;
import com.ingenio.notification.repository.SubjectTypeMasterRepository;
import com.ingenio.notification.repository.SubjectTypeRepository;
import com.ingenio.notification.repository.SubjectWithPatternRepository;
import com.ingenio.notification.repository.SubjectWithTypeRepository;
import com.ingenio.notification.repository.SyllabusChapterMasterRepository;
import com.ingenio.notification.repository.TypeOfUnitRepository;
import com.ingenio.notification.repository.YearMasterRepository;
import com.ingenio.notification.service.InitialSubjectSettingService;
import com.ingenio.notification.utility.Utility;

@Service
public class InitialSubjectSettingServiceImpl implements InitialSubjectSettingService{

	@Autowired
	GlobalStandardMasterRepository globalStandardMasterRepository;
	
	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	TypeOfUnitRepository typeOfUnitRepository;
	
	@Autowired
	StandardMasterRepository standardMasterRepository;
	
	@Autowired
	GlobalSubjectMasterRepository globalSubjectMasterRepository;
	
	@Autowired
	ExamGroupRepository examGroupRepository;
	
	@Autowired
	ExamSubGroupRepository examSubGroupRepository;
	
	@Autowired
	SemesterMasterRepository semesterMasterRepository;
	
	@Autowired
	YearMasterRepository yearMasterRepository;
	
	@Autowired
	SubjectRepository subjectRepository;
	
	@Autowired
	GlobalChapterRepository globalChapterRepository;
	
	@Autowired
	SyllabusChapterMasterRepository syllabusChapterMasterRepository;
	
	@Autowired
	ExamSubToStdMappingRepository examSubToStdMappingRepository;
	
	@Autowired
	SubjectPatternRepository subjectPatternRepository;
	
	@Autowired
	SubjectTypeMasterRepository subjectTypeMasterRepository;
	
	@Autowired
	SubjectTypeRepository subjectTypeRepository;
	
	@Autowired
	SubjectWithPatternRepository subjectWithPatternRepository;
	
	@Autowired
	SubjectWithTypeRepository subjectWithTypeRepository;
	@Override
	public Integer saveStandardFromGlobal(Integer[] globalStandatdId, Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			if(globalStandatdId.length>0) {
				List<Integer> Ids = Arrays.stream(globalStandatdId).collect(Collectors.toList());
				List<GlobalStandardMasterModel> globalStandardList = globalStandardMasterRepository.findAllById(Ids);
				Integer typeOfUnitId = InsertIntoUnitTypeIdTab(userId,schoolId);
				if(typeOfUnitId != 0) {
					SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
					globalStandardList.stream().forEach(standard->{
						StandardMasterModel standardMasterModel = new StandardMasterModel();
						
						String incrementedId = "" + standardMasterRepository.getMaxId(schoolId);
						String standardId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
						
						standardMasterModel.setStandardName(standard.getStandardName());
						standardMasterModel.setAgeRange(standard.getAgeRange());
						standardMasterModel.setStandardPriority(standard.getStandardPriority());
						
						TypeOfUnitModel typeOfUnitModel = new TypeOfUnitModel();
						typeOfUnitModel.setTypeUnitId(typeOfUnitId);
						standardMasterModel.setTypeOfUnitModel(typeOfUnitModel);
						
						standardMasterModel.setSchoolMasterModel(schoolModel);
						
						AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
						appUserRoleModel.setAppUserRoleId(userId);
						standardMasterModel.setUserId(appUserRoleModel);
						
						standardMasterModel.setIsNewAdmissionOnly("0");
						standardMasterModel.setDeviceType("1");
						
						standardMasterModel.setGlobalStandardMasterModel(standard);
						
						standardMasterModel.setStandardId(Integer.parseInt(standardId));
						
						standardMasterRepository.saveAndFlush(standardMasterModel);
					});
				}
			}
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	private Integer InsertIntoUnitTypeIdTab(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			TypeOfUnitModel typeOfUnitModel = new TypeOfUnitModel();
			String typeOfUnitId = "0";

			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
			
			String incrementedId = "" + typeOfUnitRepository.getMaxId(schoolId);
			typeOfUnitId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
			
			typeOfUnitModel.setType("Unit 1");
			typeOfUnitModel.setSchoolMasterModel(schoolModel);
			
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			typeOfUnitModel.setUserId(appUserRoleModel);
			
			typeOfUnitModel.setIsDel("0");
			typeOfUnitModel.setIsEdit("0");
			typeOfUnitModel.setIsApproval("0");
			typeOfUnitModel.setDeviceType("1");
			typeOfUnitModel.setSinkingFlag("0");
			
			if (StringUtils.isNotEmpty(typeOfUnitId) && !typeOfUnitId.equals("0")) {
				typeOfUnitModel.setTypeUnitId(Integer.parseInt(typeOfUnitId));
			}
			
			return typeOfUnitRepository.saveAndFlush(typeOfUnitModel).getTypeUnitId();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public Integer saveSubjectFromGlobal(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
				InsertIntoYearMaster(userId,schoolId);
				InsertIntoStandardMaster(userId,schoolId);
				InsertIntoSemesterTable(userId,schoolId);
				InsertIntoGroupProperties(userId,schoolId);
				InserIntoExamSubGroupProperties(userId,schoolId);
				InsertIntoSubjectPattern(userId,schoolId);
				InsertIntoSubjectTypeMaster(userId,schoolId);
				InsertIntoSubjectType(userId,schoolId);
				InsertSubjectTable(userId,schoolId);
//				InsertIntoSubjectWithPattern(userId,schoolId);
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}
	
	private void InsertIntoSubjectType(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try{
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<SubjectTypeModel> subjectTypeList = subjectTypeRepository.getSubjectTypeList(defaultSchoolId);
			if(subjectTypeList.size()>0) {
				subjectTypeList.stream().forEach(subjectTypeModel->{
					SubjectTypeModel subjectTypeModel1 = new SubjectTypeModel();
					subjectTypeModel1.setSchoolMasterModel(schoolModel);
					subjectTypeModel1.setStatus(subjectTypeModel.getStatus());
					subjectTypeModel1.setSubjectTypeName(subjectTypeModel.getSubjectTypeName());
					subjectTypeModel1.setcDate(new Date());
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					subjectTypeModel1.setUserId(appUserRoleModel);
					SubjectMasterTypeMasterModel subjectMasterTypeMasterModel = subjectTypeMasterRepository.getSubjectMasterDetails(schoolModel.getSchoolid(),subjectTypeModel.getSubjectMasterTypeMasterModel().getSubjectTypeName());
					if(subjectMasterTypeMasterModel != null) {
						subjectTypeModel1.setSubjectMasterTypeMasterModel(subjectMasterTypeMasterModel);
						subjectTypeModel1.setSubjectMasterTypeName(subjectTypeModel.getSubjectMasterTypeName());
					}
					List<SubjectTypeModel> subjectType = subjectTypeRepository.findData(schoolModel.getSchoolid(),subjectTypeModel.getSubjectTypeName());
					if(subjectType.size()>0) {
						subjectTypeModel1.setIsEdit("1");
						subjectTypeModel1.setEditBy(appUserRoleModel);
						subjectTypeModel1.setSubjectTypeId(subjectType.get(0).getSubjectTypeId());
					}else {
						String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
						String incrementedId1 = "" + subjectTypeRepository.getMaxId(schoolId);
						String patternId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
						subjectTypeModel1.setSubjectTypeId(Integer.parseInt(patternId));
					}
					subjectTypeRepository.saveAndFlush(subjectTypeModel1);
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void InsertIntoSubjectTypeMaster(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
		List<SubjectMasterTypeMasterModel> subjectTypeList = subjectTypeMasterRepository.getSubjectTypeList(defaultSchoolId);
		if(subjectTypeList.size()>0) {
			subjectTypeList.stream().forEach(subjectTypeModel->{
				SubjectMasterTypeMasterModel subjectTypeModel1 = new SubjectMasterTypeMasterModel();
				subjectTypeModel1.setSubjectTypeName(subjectTypeModel.getSubjectTypeName());
				subjectTypeModel1.setSchoolMaster(schoolModel);
				AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
				appUserRoleModel.setAppUserRoleId(userId);
				subjectTypeModel1.setUserId(appUserRoleModel);
				subjectTypeModel1.setcDate(new Date());
				List<SubjectMasterTypeMasterModel> typeList = subjectTypeMasterRepository.checkAlreadyExist(subjectTypeModel.getSubjectTypeName(),schoolModel.getSchoolid());
				if(typeList.size()>0) {
					subjectTypeModel1.setIsEdit("1");
					subjectTypeModel1.setEditBy(appUserRoleModel);
					subjectTypeModel1.setUpdatedDate(new Date());
					subjectTypeModel1.setSubjectTypeId(typeList.get(0).getSubjectTypeId());
				}else {
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
					String incrementedId1 = "" + subjectTypeMasterRepository.getMaxId(schoolId);
					String patternId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
					subjectTypeModel1.setSubjectTypeId(Integer.parseInt(patternId));
					subjectTypeModel1.setCreatedDate(new Date());
				}
				subjectTypeMasterRepository.saveAndFlush(subjectTypeModel1);
			});
		}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void InsertIntoSubjectPattern(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<SubjectPatternModel> subjectPatternList = subjectPatternRepository.getPatternsList(defaultSchoolId);
			if(subjectPatternList.size()>0) {
				subjectPatternList.stream().forEach(subjectPatternModel->{
					SubjectPatternModel subjectPatternModel1 = new SubjectPatternModel();
					subjectPatternModel1.setPatternName(subjectPatternModel.getPatternName());
					subjectPatternModel1.setPatternType(subjectPatternModel.getPatternType());
					subjectPatternModel1.setStatus(subjectPatternModel.getStatus());
					subjectPatternModel1.setSchoolMaster(schoolModel);
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					subjectPatternModel1.setUserId(appUserRoleModel);
					subjectPatternModel1.setcDate(new Date());
					List<SubjectPatternModel> PatternList = subjectPatternRepository.CheckAlreadyExist(subjectPatternModel.getPatternName(),schoolModel.getSchoolid(),subjectPatternModel.getPatternType());
					if(PatternList.size()>0) {
						subjectPatternModel1.setIsEdit("1");
						subjectPatternModel1.setEditBy(appUserRoleModel);
						subjectPatternModel1.setEditDate(new Date());
						subjectPatternModel1.setPatternId(PatternList.get(0).getPatternId());
					}else {
						String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
						String incrementedId1 = "" + subjectPatternRepository.getMaxId(schoolId);
						String patternId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
						subjectPatternModel1.setPatternId(Integer.parseInt(patternId));
					}
					subjectPatternRepository.saveAndFlush(subjectPatternModel1);
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void InsertIntoStandardMaster(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub

		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<StandardMasterModel> standardMasterModel = standardMasterRepository.getStandardByDefaultSchool(defaultSchoolId);
			if(standardMasterModel.size()>0) {
				standardMasterModel.stream().forEach(standardModel->{
					StandardMasterModel standardMasterModel1 = new StandardMasterModel();
					
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
					
					String incrementedId1 = "" + standardMasterRepository.getMaxId(schoolId);
					String yearId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
					
					standardMasterModel1.setStandardName(standardModel.getStandardName());
					standardMasterModel1.setAgeRange(standardModel.getAgeRange());
					standardMasterModel1.setStandardPriority(standardModel.getStandardPriority());
					
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					standardMasterModel1.setUserId(appUserRoleModel);
					
					standardMasterModel1.setSchoolMasterModel(schoolModel);
					
					StandardMasterModel standardMasterModel11 = standardMasterRepository.getAlreadyExistStandard(standardModel.getStandardName(),schoolId);
					if(standardMasterModel11 != null) {
						standardMasterModel1.setIsEdit("1");
						standardMasterModel1.setEditBy(appUserRoleModel);
						standardMasterModel1.setEditDate(new Date());
						standardMasterModel1.setStandardId(standardMasterModel11.getStandardId());
					}else {
						standardMasterModel1.setStandardId(Integer.parseInt(yearId));
					}
		
					standardMasterRepository.saveAndFlush(standardMasterModel1);
					
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
	
	}

	private void InsertIntoSyllabusChapter(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		List<GlobalChapterModel> globalChapterList = globalChapterRepository.findAll();
		for (GlobalChapterModel globalChapterModel : globalChapterList) {
			SubjectModel subject = subjectRepository.getSubjectList(globalChapterModel.getGlobalSubjectMasterModel().getSubjectName(),schoolId);
			SyllabusChapterMasterModel syllabusChapterMasterModel = new SyllabusChapterMasterModel();
			syllabusChapterMasterModel.setSchoolId(schoolModel);
			syllabusChapterMasterModel.setSubject(subject);
			syllabusChapterMasterModel.setSyllChapterName(globalChapterModel.getChapterName());
			syllabusChapterMasterModel.setChapterNo(globalChapterModel.getChapterNo());
			
			AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
			appUserRoleModel.setAppUserRoleId(userId);
			syllabusChapterMasterModel.setAppUserRole(appUserRoleModel);
			
			List<SyllabusChapterMasterModel> syllabusChapterList = syllabusChapterMasterRepository.CheckAlreadyExist(globalChapterModel.getChapterName(),subject.getSubjectId(),schoolId);
			if(syllabusChapterList.size()>0) {
				syllabusChapterMasterModel.setIsEdit("1");
				syllabusChapterMasterModel.setEditBy(appUserRoleModel);
				syllabusChapterMasterModel.setEditDate(new Date());
				syllabusChapterMasterModel.setSyllChapterId(syllabusChapterList.get(0).getSyllChapterId());
			}else {
				String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
				String incrementedId1 = "" + syllabusChapterMasterRepository.getMaxId(schoolId);
				String tabId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
				syllabusChapterMasterModel.setSyllChapterId(Integer.parseInt(tabId));
			}
			syllabusChapterMasterRepository.saveAndFlush(syllabusChapterMasterModel);
			
		}
		
	}

	private void InsertSubjectTable(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<SubjectModel> subjectModelList = subjectRepository.getSubjectListByDefaultSchoolId(defaultSchoolId);
			if(subjectModelList.size()>0) {
				subjectModelList.stream().forEach(subjectList->{
					if(subjectList.getStandardMasterModel() != null) {
						SubjectModel subjectModel = new SubjectModel();
						
						String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
						String incrementedId1 = "" + subjectRepository.getMaxSubjectId(schoolId);
						String subjectId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
						if(subjectList.getExamGroupModel() != null && subjectList.getExamGroupModel().getGroupId() != 0) {
							ExamGroupModel examModel = examGroupRepository.getGroupModel(subjectList.getExamGroupModel().getGroupName(), schoolId);
							subjectModel.setExamGroupModel(examModel);
						}
						
						if(subjectList.getExamSubGroupModel() != null && subjectList.getExamSubGroupModel().getSubGroupId() != 0) {
							List<ExamSubGroupModel> examSubGroupModel = examSubGroupRepository.getSubGroupModel(subjectList.getExamSubGroupModel().getSubGroupName(),schoolId);
							subjectModel.setExamSubGroupModel(examSubGroupModel.get(0));
						}
						
						StandardMasterModel standardModel = subjectRepository.getStandardModel(subjectList.getStandardMasterModel().getStandardName(),schoolId);
						subjectModel.setStandardMasterModel(standardModel);
						
						YearMasterModel yearMasterModel = subjectRepository.getYearMasterModel(subjectList.getYearMasterModel().getYear(),schoolId);
						subjectModel.setYearMasterModel(yearMasterModel);
						if(subjectList.getSemesterMasterModel() != null) {
							SemesterMasterModel semesterMasterModel = examGroupRepository.getSemesterMasterModel(subjectList.getSemesterMasterModel().getSemesterName(), schoolId);
							subjectModel.setSemesterMasterModel(semesterMasterModel);
						}
						
						subjectModel.setCourseCode(subjectList.getCourseCode());
						subjectModel.setCompulElectiveFlag(subjectList.getCompulElectiveFlag());
						subjectModel.setCreatedDate(new Date().toString());
						subjectModel.setStatus(subjectList.getStatus());
						subjectModel.setCredit(subjectList.getCredit());
						subjectModel.setCreditWithoutAttendance(subjectList.getCreditWithoutAttendance());
						subjectModel.setGradeType(subjectList.getGradeType());
						subjectModel.setIsEligibleGrace(subjectList.getIsEligibleGrace());
						subjectModel.setIsEligibleGrade(subjectList.getIsEligibleGrade());
						subjectModel.setGlobalSubjectMasterModel(subjectList.getGlobalSubjectMasterModel());
						subjectModel.setPracticaltheoryFlag(subjectList.getPracticaltheoryFlag());
						subjectModel.setYearsemesterflag(subjectList.getYearsemesterflag());
						subjectModel.setSubjectCourseFlag(subjectList.getSubjectCourseFlag());
						subjectModel.setRcAmount(subjectList.getRcAmount());
						subjectModel.setRwcAmount(subjectList.getRwcAmount());
						subjectModel.setPriorityForMarksheet(subjectList.getPriorityForMarksheet());		
						AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
						appUserRoleModel.setAppUserRoleId(userId);
						subjectModel.setUserId(appUserRoleModel);
						
						subjectModel.setSchoolMasterModel(schoolModel);
						
						subjectModel.setSubjectName(subjectList.getSubjectName());
						
						List<SubjectModel> SubjectModelList1 = subjectRepository.CheckAlreadyExist(standardModel.getStandardId(),yearMasterModel.getYearId(),schoolId,subjectList.getSubjectName());
						if(SubjectModelList1.size()>0) {
							subjectModel.setEditBy(appUserRoleModel);
							subjectModel.setIsEdit("1");
							subjectModel.setEditDate(new Date());
							subjectModel.setSubjectId(SubjectModelList1.get(0).getSubjectId());
						}else {
							subjectModel.setSubjectId(Integer.parseInt(subjectId));
						}
						SubjectModel finalSubjectModel =subjectRepository.saveAndFlush(subjectModel);
						if(subjectModel.getGlobalSubjectMasterModel() != null) {
							List<GlobalChapterModel> globalChapterList = globalChapterRepository.findByGlobalSubject(subjectModel.getGlobalSubjectMasterModel().getGlobalSubjectId());
							for (GlobalChapterModel globalChapterModel : globalChapterList) {
								SyllabusChapterMasterModel syllabusChapterMasterModel = new SyllabusChapterMasterModel();
								syllabusChapterMasterModel.setSchoolId(schoolModel);
								syllabusChapterMasterModel.setSubject(finalSubjectModel);
								syllabusChapterMasterModel.setSyllChapterName(globalChapterModel.getChapterName());
								syllabusChapterMasterModel.setChapterNo(globalChapterModel.getChapterNo());
								
								List<SyllabusChapterMasterModel> syllabusChapterList = syllabusChapterMasterRepository.CheckAlreadyExist(globalChapterModel.getChapterName(),finalSubjectModel.getSubjectId(),schoolId);
								if(syllabusChapterList.size()>0) {
									syllabusChapterMasterModel.setIsEdit("1");
									syllabusChapterMasterModel.setEditBy(appUserRoleModel);
									syllabusChapterMasterModel.setEditDate(new Date());
									syllabusChapterMasterModel.setSyllChapterId(syllabusChapterList.get(0).getSyllChapterId());
								}else {
									String globalDbSchoolKey1 = "1".concat("" + schoolModel.getSchoolKey());
									String incrementedId11 = "" + syllabusChapterMasterRepository.getMaxId(schoolId);
									String tabId = Utility.generatePrimaryKey(globalDbSchoolKey1, incrementedId11);
									syllabusChapterMasterModel.setSyllChapterId(Integer.parseInt(tabId));
								}
								syllabusChapterMasterRepository.saveAndFlush(syllabusChapterMasterModel);
								
							}
						}
						if(subjectList.getSubjectCourseFlag().equalsIgnoreCase("1")) {
							ExamSubToStdMapping examSubToStdMapping  =new ExamSubToStdMapping();
							examSubToStdMapping.setSubjectMaster(finalSubjectModel);
							examSubToStdMapping.setStandardmaster(finalSubjectModel.getStandardMasterModel());
							examSubToStdMapping.setSchoolMaster(finalSubjectModel.getSchoolMasterModel());
							examSubToStdMapping.setUserId(finalSubjectModel.getUserId());
							examSubToStdMapping.setcDate(new Date());
							List<ExamSubToStdMapping> tabList = examSubToStdMappingRepository.CheckAlreadyExist(finalSubjectModel.getStandardMasterModel().getStandardId(),finalSubjectModel.getSubjectId(),finalSubjectModel.getSchoolMasterModel().getSchoolid());
							if(tabList.size()>0) {
								examSubToStdMapping.setIsEdit("1");
								examSubToStdMapping.setEditBy(appUserRoleModel);
								examSubToStdMapping.setEditDate(new Date());
								examSubToStdMapping.setSubToStdMappingId(tabList.get(0).getSubToStdMappingId());
							}else {
								String globalDbSchoolKey1 = "1".concat("" + schoolModel.getSchoolKey());
								String incrementedId11 = "" + examSubToStdMappingRepository.getMaxId(schoolId);
								String tabId = Utility.generatePrimaryKey(globalDbSchoolKey1, incrementedId11);
								examSubToStdMapping.setSubToStdMappingId(Integer.parseInt(tabId));
							}
							examSubToStdMappingRepository.saveAndFlush(examSubToStdMapping);
						}
						List<SubjectWithPatternModel> subjectPatternList = subjectWithPatternRepository.findDataBySubject(subjectList.getSchoolMasterModel().getSchoolid(),subjectList.getSubjectId());
						SubjectPatternModel subjectPatternModel = subjectPatternRepository.getSubjectPattern(schoolModel.getSchoolid(),subjectPatternList.get(0).getSubjectPatternModel().getPatternName(),subjectPatternList.get(0).getSubjectPatternModel().getPatternType());
						SubjectWithPatternModel subjectWithPatternModel = new SubjectWithPatternModel();
						subjectWithPatternModel.setSchoolMasterModel(schoolModel);
						subjectWithPatternModel.setSubjectModel(finalSubjectModel);
						subjectWithPatternModel.setSubjectPatternModel(subjectPatternModel);
						appUserRoleModel.setAppUserRoleId(userId);
						subjectWithPatternModel.setUserId(appUserRoleModel);
						subjectWithPatternModel.setcDate(new Date());
						List<SubjectWithPatternModel> subjectPatternList1 = subjectWithPatternRepository.findData(schoolModel.getSchoolid(),finalSubjectModel.getSubjectId(),subjectPatternModel.getPatternId());
						if(subjectPatternList1.size()>0) {
							subjectWithPatternModel.setIsEdit("1");
							subjectWithPatternModel.setEditBy(appUserRoleModel);
							subjectWithPatternModel.setEditDate(new Date());
							subjectWithPatternModel.setSubjectPatternId(subjectPatternList1.get(0).getSubjectPatternId());
						}else {
							String globalDbSchoolKey1 = "1".concat("" + schoolModel.getSchoolKey());
							String incrementedId11 = "" + subjectWithPatternRepository.getMaxId(schoolId);
							String patternId = Utility.generatePrimaryKey(globalDbSchoolKey1, incrementedId11);
							subjectWithPatternModel.setSubjectPatternId(Integer.parseInt(patternId));
						}
						subjectWithPatternRepository.saveAndFlush(subjectWithPatternModel);
						
//						List<SubjectWithTypeModel> subjectTypeList = subjectWithTypeRepository.findData(subjectList.getSubjectId());
//						SubjectTypeModel subjectTypeModel = subjectTypeRepository.getOne(subjectTypeList.get(0).getSubjectTypeId());
//						List<SubjectTypeModel> subjectType = subjectTypeRepository.findDataNew(schoolModel.getSchoolid(),subjectTypeModel.getSubjectTypeName());
//						
//						SubjectWithTypeModel subjectWithTypeModel = new SubjectWithTypeModel();
//						subjectWithTypeModel.setSubjectModel(finalSubjectModel);
//						subjectWithTypeModel.setSubjectTypeId(subjectType.get(0).getSubjectTypeId());
//						
//						subjectWithTypeRepository.saveAndFlush(subjectWithTypeModel);
					}
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	private void InserIntoExamSubGroupProperties(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		
		SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
		Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
		
		List<ExamSubGroupModel> examSubGroupModel = examGroupRepository.getSubGroupModelBySchoolId(defaultSchoolId);
		if(examSubGroupModel.size()>0) {
			examSubGroupModel.stream().forEach(subGroupModel->{
				ExamSubGroupModel examSubModel = new ExamSubGroupModel();
				
				String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
				
				String incrementedId1 = "" + examSubGroupRepository.getMaxSubGroupId(schoolId);
				String examSubgroupGroupIdId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
				
				examSubModel.setSubGroupName(subGroupModel.getSubGroupName());
				examSubModel.setMaxSubjects(subGroupModel.getMaxSubjects());
				examSubModel.setMinSubjects(subGroupModel.getMinSubjects());
				examSubModel.setSchoolMasterModel(schoolModel);
//				System.out.println(""+subGroupModel.getExamGroupModel().getGroupName()+" "+schoolId);
				ExamGroupModel examGroupModel1 = examGroupRepository.getGroupModel(subGroupModel.getExamGroupModel().getGroupName(),schoolId);
				examSubModel.setExamGroupModel(examGroupModel1);
				
				examSubModel.setSelectionType(subGroupModel.getSelectionType());
				
				AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
				appUserRoleModel.setAppUserRoleId(userId);
				examSubModel.setUserId(appUserRoleModel);
				
				examSubModel.setIsCompulsory(subGroupModel.getIsCompulsory());
				List<ExamSubGroupModel> exammGModel = examSubGroupRepository.getAlreadyExist(examGroupModel1.getGroupId(),subGroupModel.getSubGroupName(),schoolId);
				
				if(exammGModel.size()>0) {
					examSubModel.setIsEdit("1");
					examSubModel.setEditBy(appUserRoleModel);
					examSubModel.setEditDate(new Date());
					examSubModel.setSubGroupId(exammGModel.get(0).getSubGroupId());
				}else {
					examSubModel.setSubGroupId(Integer.parseInt(examSubgroupGroupIdId));
				}
				examSubGroupRepository.saveAndFlush(examSubModel);
				
			});
		}
	}

	private void InsertIntoYearMaster(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<YearMasterModel> yearMasterModel = yearMasterRepository.getYearByDefaultSchool(defaultSchoolId);
			if(yearMasterModel.size()>0) {
				yearMasterModel.stream().forEach(yearModel->{
					YearMasterModel yearMasterModel1 = new YearMasterModel();
					
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
					
					String incrementedId1 = "" + yearMasterRepository.getMaxYearId(schoolId);
					String yearId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
					
					yearMasterModel1.setYear(yearModel.getYear());
					yearMasterModel1.setIsCurrentAcadamicYear(yearModel.getIsCurrentAcadamicYear());
					
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					yearMasterModel1.setUserId(appUserRoleModel);
					
					yearMasterModel1.setSchoolMasterModel(schoolModel);
					
					YearMasterModel yearMasterModel11 = yearMasterRepository.getAlreadyExistYear(yearModel.getYear(),schoolId);
					if(yearMasterModel11 != null) {
						yearMasterModel1.setIsEdit("1");
						yearMasterModel1.setEditBy(appUserRoleModel);
						yearMasterModel1.setEditDate(new Date());
						yearMasterModel1.setYearId(yearMasterModel11.getYearId());
					}else {
						yearMasterModel1.setYearId(Integer.parseInt(yearId));
					}
		
					yearMasterRepository.saveAndFlush(yearMasterModel1);
					
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
	}

	private void InsertIntoSemesterTable(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			List<SemesterMasterModel> semesterMasterModel1 = semesterMasterRepository.getSemesterByGroupId(defaultSchoolId);
			if(semesterMasterModel1.size()>0) {
				semesterMasterModel1.stream().forEach(semesterModel->{
					SemesterMasterModel semesterMasterModel11 = new SemesterMasterModel();
					
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
					
					String incrementedId1 = "" + semesterMasterRepository.getMaxSemesterId(schoolId);
					String semesterId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId1);
					
					semesterMasterModel11.setSemesterName(semesterModel.getSemesterName());
					semesterMasterModel11.setSemesterType(semesterModel.getSemesterType());
					semesterMasterModel11.setOrder(semesterModel.getOrder());
					
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					semesterMasterModel11.setUserId(appUserRoleModel);
					
					semesterMasterModel11.setSchoolMasterModel(schoolModel);
					
					SemesterMasterModel semesterMModel = semesterMasterRepository.getAlreadyExistSemester(semesterModel.getSemesterName(),schoolId);
					if(semesterMModel != null) {
						semesterMasterModel11.setIsEdit("1");
						semesterMasterModel11.setEditBy(appUserRoleModel);
						semesterMasterModel11.setEditDate(new Date());
						semesterMasterModel11.setSemesterId(semesterMModel.getSemesterId());
					}else {
						semesterMasterModel11.setSemesterId(Integer.parseInt(semesterId));
					}
					
					semesterMasterRepository.saveAndFlush(semesterMasterModel11);
					
				});
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void InsertIntoGroupProperties(Integer userId, Integer schoolId) {
		// TODO Auto-generated method stub
		try {
			SchoolMasterModel schoolModel = schoolMasterRepository.findBySchoolid(schoolId);
			Integer defaultSchoolId =schoolModel.getSchoolBoardModel().getSchoolId();
			
			List<ExamGroupModel> examGroupModel = examGroupRepository.getModelBySchoolId(defaultSchoolId);
			if(examGroupModel.size()>0) {
				examGroupModel.stream().forEach(groupModel->{
					String globalDbSchoolKey = "1".concat("" + schoolModel.getSchoolKey());
				
					ExamGroupModel examModel = new ExamGroupModel();
					YearMasterModel yearMasterModel = examGroupRepository.getYearMasterModel(groupModel.getYearMasterModel().getYear(),schoolId);
					StandardMasterModel standaMasterModel = examGroupRepository.getStandardMasterModel(groupModel.getStandardMasterModel().getStandardName(),schoolId);
					SemesterMasterModel semesterMasterModel = examGroupRepository.getSemesterMasterModel(groupModel.getSemesterMasterModel().getSemesterName(),schoolId);
					
					String incrementedId = "" + examGroupRepository.getMaxGroupId(schoolId);
					String examGroupIdId = Utility.generatePrimaryKey(globalDbSchoolKey, incrementedId);
					examModel.setGroupName(groupModel.getGroupName());
					examModel.setYearMasterModel(yearMasterModel);
					examModel.setStandardMasterModel(standaMasterModel);
					examModel.setSemesterMasterModel(semesterMasterModel);
					examModel.setSchoolMasterModel(schoolModel);
					examModel.setMaxSubGroups(groupModel.getMaxSubGroups());
					examModel.setMinSubGroups(groupModel.getMinSubGroups());
					examModel.setTotalSubjects(groupModel.getTotalSubjects());
					examModel.setSelectionType(groupModel.getSelectionType());
					
					AppUserRoleModel appUserRoleModel = new AppUserRoleModel();
					appUserRoleModel.setAppUserRoleId(userId);
					examModel.setUserId(appUserRoleModel);
					List<ExamGroupModel> exammGroupModel = examGroupRepository.getAlreadyExistGroup(yearMasterModel.getYearId(),standaMasterModel.getStandardId(),semesterMasterModel.getSemesterId(),groupModel.getGroupName(),schoolId);
					if(exammGroupModel.size()>0) {
						examModel.setIsEdit("1");
						examModel.setEditBy(appUserRoleModel);
						examModel.setEditDate(new Date());
						examModel.setGroupId(exammGroupModel.get(0).getGroupId());
					}else {
						examModel.setGroupId(Integer.parseInt(examGroupIdId));
					}
					
					examGroupRepository.saveAndFlush(examModel);
					
				});
				
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
