package com.ingenio.notification.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.ingenio.notification.bean.FCMTokenBean;
import com.ingenio.notification.bean.NotificationBean;
import com.ingenio.notification.bean.WhatsAppMessageBean;
import com.ingenio.notification.model.SaveSendNotificationModel;
import com.ingenio.notification.model.SaveSendWhatsAppSMSModel;
import com.ingenio.notification.repository.GlobalWhatsAppSMSRepository;
import com.ingenio.notification.repository.SaveSendNotificationRepository;
import com.ingenio.notification.repository.SaveSendWhatsAppSMSRepository;
import com.ingenio.notification.repository.SchoolMasterRepository;
import com.ingenio.notification.service.SendWhatsAppMessageService;
import com.ingenio.notification.utility.Utility;

@Component
public class SendWhatsAppMessageServiceImpl implements SendWhatsAppMessageService{

	@Autowired
	SchoolMasterRepository schoolMasterRepository;
	
	@Autowired
	GlobalWhatsAppSMSRepository globalWhatsAppSMSRepository;
	
	@Autowired
	SaveSendWhatsAppSMSRepository saveSendWhatsAppSMSRepository;
	
	@Autowired
	SaveSendNotificationRepository saveSendNotificationRepository;
	
	@Override
	public void sendWhatsAppSMS(WhatsAppMessageBean whatsAppMessageBean) {
		// TODO Auto-generated method stub
		try {
				if(CollectionUtils.isNotEmpty(whatsAppMessageBean.getSendingList())) {
					List<WhatsAppMessageBean> messageList=whatsAppMessageBean.getSendingList();
					for (WhatsAppMessageBean message : messageList) {
						sendMessage(message);
					}
				}else {
					sendMessage(whatsAppMessageBean);
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}

	private void sendMessage(WhatsAppMessageBean whatsAppMessageBean) {
		// TODO Auto-generated method stub
		try {
			boolean flag=false;
			if(whatsAppMessageBean.getAssignMsgCount() == null) {
				flag=true;
			}else {
				if(Integer.parseInt(whatsAppMessageBean.getAssignMsgCount())>0) {
					flag=true;
				}
			}
			if(flag) {
				String newMobile ="91"+Utility.convertOtherToEnglish(whatsAppMessageBean.getMobileNo());
				URL obj = new URL("https://app.wappapi.in/api/send.php?number="+newMobile+"&type=text&message="+whatsAppMessageBean.getEncryptedmessage()+"&instance_id="+whatsAppMessageBean.getInstanceId()+"&access_token="+whatsAppMessageBean.getAccessToken()+"");
				System.out.println(""+obj);
				HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
				postConnection.addRequestProperty("User-Agent", "Chrome");
				postConnection.setRequestMethod("POST");
				postConnection.setDoOutput(true);
				postConnection.connect();
				@SuppressWarnings("unused")
				int responseCode = postConnection.getResponseCode();
				if(postConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
				    String inputLine;
				    StringBuilder response = new StringBuilder();
				    while ((inputLine = in .readLine()) != null) {
				        response.append(inputLine);
				    } in .close();
				    if(!response.toString().equalsIgnoreCase(null) && response.toString() != null) {
				    	JSONObject jsonObj = new JSONObject(response.toString());
					    if(!jsonObj.getString("status").equalsIgnoreCase("error")) {
					    	SaveSendWhatsAppSMSModel saveSendWhatsAppSMSModel = new SaveSendWhatsAppSMSModel();
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							Date dateobj = new Date();
							String date = df.format(dateobj);
							saveSendWhatsAppSMSModel.setDate(date);
							saveSendWhatsAppSMSModel.setMessage(whatsAppMessageBean.getMessage());
							saveSendWhatsAppSMSModel.setMessageType(String.valueOf(whatsAppMessageBean.getMessageType()));
							saveSendWhatsAppSMSModel.setTypeFlag(whatsAppMessageBean.getTypeFlag());
							saveSendWhatsAppSMSModel.setRenewStaffId(whatsAppMessageBean.getRenewId());
							saveSendWhatsAppSMSModel.setSchoolId(whatsAppMessageBean.getSchoolId());
							saveSendWhatsAppSMSModel.setMobileNo(whatsAppMessageBean.getMobileNo());
							saveSendWhatsAppSMSRepository.saveAndFlush(saveSendWhatsAppSMSModel);
					    }
					    if(whatsAppMessageBean.getAssignMsgCount()!=null) {
							Integer totalCount=Integer.parseInt(whatsAppMessageBean.getAssignMsgCount())-1;
							globalWhatsAppSMSRepository.updateSchoolAssignMessage(whatsAppMessageBean.getSchoolId(),totalCount.toString());
						}
				    }
				    
				}
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void sendSMS(WhatsAppMessageBean whatsAppMessageBean) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendNotification(WhatsAppMessageBean whatsAppMessageBean,Integer isBroadcast) {
		// TODO Auto-generated method stub
		try {
			if(whatsAppMessageBean.getCustomizeMessageFlag() == 0) {
				if(isBroadcast == 1) {
					List<String> tokenList = new ArrayList<>();
					for (FCMTokenBean token : whatsAppMessageBean.getTokenList()) {
						tokenList.add(token.getToken());
					}
					try {
						Notification notification = Notification.builder().setTitle(whatsAppMessageBean.getTitle()).setBody(whatsAppMessageBean.getMessage()).build();
						MulticastMessage message = MulticastMessage.builder()
							    .setNotification(notification)
							    .putData("title", whatsAppMessageBean.getTitle())
							    .putData("body", whatsAppMessageBean.getMessage())
							    .addAllTokens(tokenList)
							    .build();
						BatchResponse response;
						try {
								response = FirebaseMessaging.getInstance().sendMulticast(message);
								System.out.println(response.getSuccessCount() + " messages were sent successfully");
						} catch (FirebaseMessagingException e) {
								// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					
				}else {
					List<FCMTokenBean> tokenList = whatsAppMessageBean.getTokenList();
					for (FCMTokenBean token : tokenList) {
						if(!token.getToken().equalsIgnoreCase(null) && !token.getToken().equalsIgnoreCase("")) {
							String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
							URL url = new URL(FIREBASE_API_URL);
							HttpURLConnection conn = (HttpURLConnection) url.openConnection();

							conn.setUseCaches(false);
							conn.setDoInput(true);
							conn.setDoOutput(true);

							conn.setRequestMethod("POST");
							conn.setRequestProperty("Authorization", "key=" + whatsAppMessageBean.getServerKey());
							conn.setRequestProperty("Content-Type", "application/json");
							
							try {							
								JSONObject json = new JSONObject();
								json.put("to", token.getToken());

								JSONObject data = new JSONObject();
								data.put("mobileNo", token.getMobileNo());
								data.put("userId", token.getUserId().toString());
								data.put("menuId", token.getMenuId().toString());
								data.put("menuName", token.getMenuName());
								data.put("title", whatsAppMessageBean.getTitle());
								data.put("body", whatsAppMessageBean.getMessage());
								data.put("SeenStataus", "0");
								json.put("data", data);
								
//								JSONObject info = new JSONObject();
//								info.put("title", whatsAppMessageBean.getTitle()); // Notification title
//								info.put("body", whatsAppMessageBean.getMessage()); // Notification
////								info.put("message", "hello user"); // body
//								json.put("notification", info);
								
								OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
								wr.write(json.toString());
								wr.flush();

								BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
								String output;
								StringBuffer response = new StringBuffer();
								while ((output = br.readLine()) != null) {
									response.append(output);
								}
								br.close();
								JSONObject jsonObj=new JSONObject(response.toString());
							
								SaveSendNotificationModel saveSendNotificationModel = new SaveSendNotificationModel();
								saveSendNotificationModel.setMessageType(token.getMessageType());
								saveSendNotificationModel.setTypeFlag(token.getTypeFlag());
								saveSendNotificationModel.setRenewStaffId(token.getRenewStaffId());
								saveSendNotificationModel.setSchoolId(token.getSchoolId());
								saveSendNotificationModel.setMobileNo(token.getMobileNo());
								saveSendNotificationModel.setMessage(whatsAppMessageBean.getMessage());
								saveSendNotificationModel.setTitle(whatsAppMessageBean.getTitle());
								saveSendNotificationModel.setMenuId(token.getMenuId());
								saveSendNotificationModel.setSeenUnseenStatusFlag(0);
								saveSendNotificationModel.setNotificationId(token.getNotificationId());
								DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
								Date dateobj = new Date();
								String date = df.format(dateobj);
								saveSendNotificationModel.setDate(date);
								if(jsonObj.getInt("success") == 1) {
									saveSendNotificationModel.setSuccessFailureFlag("1");
								}else {
									saveSendNotificationModel.setSuccessFailureFlag("0");
								}
								saveSendNotificationRepository.saveAndFlush(saveSendNotificationModel);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			}else {
				List<FCMTokenBean> tokenList = whatsAppMessageBean.getTokenList();
				for (FCMTokenBean token : tokenList) {
					if(!token.getToken().equalsIgnoreCase(null) && !token.getToken().equalsIgnoreCase("")) {
						String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
						URL url = new URL(FIREBASE_API_URL);
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();

						conn.setUseCaches(false);
						conn.setDoInput(true);
						conn.setDoOutput(true);

						conn.setRequestMethod("POST");
						conn.setRequestProperty("Authorization", "key=" + whatsAppMessageBean.getServerKey());
						conn.setRequestProperty("Content-Type", "application/json");
						try {
							JSONObject json = new JSONObject();
							json.put("to", token.getToken());

							JSONObject data = new JSONObject();
							data.put("mobileNo", token.getMobileNo());
							data.put("userId", token.getUserId().toString());
							data.put("menuId", token.getMenuId().toString());
							data.put("menuName", token.getMenuName());
							data.put("title", token.getTitle());
							data.put("body", token.getMessage());
							data.put("SeenStataus", "0");
							json.put("data", data);
							
//							JSONObject info = new JSONObject();
//							info.put("title", token.getTitle()); // Notification title
//							info.put("body", token.getMessage()); // Notification
////							info.put("message", "hello user"); // body
//							json.put("notification", info);
							
							OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
							wr.write(json.toString());
							wr.flush();

							BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

							String output;
							StringBuffer response = new StringBuffer();
							while ((output = br.readLine()) != null) {
								response.append(output);
							}
							br.close();
							JSONObject jsonObj=new JSONObject(response.toString());

							SaveSendNotificationModel saveSendNotificationModel = new SaveSendNotificationModel();
							saveSendNotificationModel.setMessageType(token.getMessageType());
							saveSendNotificationModel.setTypeFlag(token.getTypeFlag());
							saveSendNotificationModel.setRenewStaffId(token.getRenewStaffId());
							saveSendNotificationModel.setSchoolId(token.getSchoolId());
							saveSendNotificationModel.setMobileNo(token.getMobileNo());
							saveSendNotificationModel.setMessage(token.getMessage());
							saveSendNotificationModel.setTitle(token.getTitle());
							saveSendNotificationModel.setMenuId(token.getMenuId());
							saveSendNotificationModel.setSeenUnseenStatusFlag(0);
							saveSendNotificationModel.setNotificationId(token.getNotificationId());
							DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
							Date dateobj = new Date();
							String date = df.format(dateobj);
							saveSendNotificationModel.setDate(date);
							if(jsonObj.getInt("success") == 1) {
								saveSendNotificationModel.setSuccessFailureFlag("1");
							}else {
								saveSendNotificationModel.setSuccessFailureFlag("0");
							}
							saveSendNotificationRepository.saveAndFlush(saveSendNotificationModel);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}

	@Override
	public List<NotificationBean> getAllNotifications(Integer renewId, String role, Integer offset) {
		// TODO Auto-generated method stub

		
		List<SaveSendNotificationModel> notificationListModel = new ArrayList<SaveSendNotificationModel>();
		if (role.equalsIgnoreCase("Parent") || role.equalsIgnoreCase("father")) {
			saveSendNotificationRepository.updateAllNotificationStatus(renewId,"1");
			notificationListModel = saveSendNotificationRepository.getAllNotifications(renewId, "1");
		} else if (role.equalsIgnoreCase("Teacher")) {
			saveSendNotificationRepository.updateAllNotificationStatus(renewId,"2");
			notificationListModel = saveSendNotificationRepository.getAllNotifications(renewId, "2");
		}
		List<NotificationBean> notificationList1 = new ArrayList<NotificationBean>();
		if (CollectionUtils.isNotEmpty(notificationListModel)) {
			notificationListModel.stream().forEach(notifi -> {
				NotificationBean notificationBean = new NotificationBean();
				notificationBean.setTabId(notifi.getTabId());
				notificationBean.setDate(notifi.getDate());
				notificationBean.setMessage(notifi.getMessage());
				notificationBean.setTitle(notifi.getTitle());
				if(notifi.getMessageType().equalsIgnoreCase("1")) {
					notificationBean.setNotificationType("Notice");
				}else if(notifi.getMessageType().equalsIgnoreCase("2")) {
					notificationBean.setNotificationType("Gallery");
				}else if(notifi.getMessageType().equalsIgnoreCase("4")) {
					notificationBean.setNotificationType("Homework");
				}else {
					notificationBean.setNotificationType("Other");
				}
				notificationBean.setMobileNo(notifi.getMobileNo());
				notificationBean.setNotificationId(notifi.getNotificationId());
				notificationBean.setSeenUnseenStatusFlag(notifi.getSeenUnseenStatusFlag());
				notificationBean.setSuccessFailureFlag(notifi.getSuccessFailureFlag());
				notificationList1.add(notificationBean);
			});
			
			return notificationList1;
		}
		
		return new ArrayList<NotificationBean>();
	
	}

	@Override
	public Integer updateNotificationSeenStatus(String notificationId) {
		// TODO Auto-generated method stub
		try {
			List<Integer> tabId = Arrays.stream(notificationId.split(",")).map(Integer::parseInt).collect(Collectors.toList());
			saveSendNotificationRepository.updateNotificationStatus(tabId);
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Integer getAllNotificationsUnseenCount(Integer renewId, String role) {
		// TODO Auto-generated method stub
		try {
			List<SaveSendNotificationModel> notificationListModel = new ArrayList<SaveSendNotificationModel>();
			if (role.equalsIgnoreCase("Parent") || role.equalsIgnoreCase("father")) {
				notificationListModel = saveSendNotificationRepository.getAllNotificationsUnseenCount(renewId, "1");
			} else if (role.equalsIgnoreCase("Teacher")) {
				notificationListModel = saveSendNotificationRepository.getAllNotificationsUnseenCount(renewId, "2");
			}else if(role.equalsIgnoreCase("Principal")) {
				notificationListModel = saveSendNotificationRepository.getAllNotificationsUnseenCount(renewId, "3");
			}
			return notificationListModel.size();
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

}
