package com.ingenio.notification.service;

import java.util.List;

import com.ingenio.notification.bean.NotificationBean;
import com.ingenio.notification.bean.WhatsAppMessageBean;

public interface SendWhatsAppMessageService {

	void sendWhatsAppSMS(WhatsAppMessageBean whatsAppMessageBean);

	void sendSMS(WhatsAppMessageBean whatsAppMessageBean);

	void sendNotification(WhatsAppMessageBean whatsAppMessageBean, Integer isBroadcast);

	List<NotificationBean> getAllNotifications(Integer renewId, String role, Integer offset);

	Integer updateNotificationSeenStatus(String notificationId);

	Integer getAllNotificationsUnseenCount(Integer renewId, String role);
	
	

}
