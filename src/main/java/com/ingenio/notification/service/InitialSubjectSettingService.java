package com.ingenio.notification.service;

public interface InitialSubjectSettingService {

	Integer saveStandardFromGlobal(Integer[] globalStandatdId, Integer userId, Integer schoolId);

	Integer saveSubjectFromGlobal(Integer userId, Integer schoolId);

}
