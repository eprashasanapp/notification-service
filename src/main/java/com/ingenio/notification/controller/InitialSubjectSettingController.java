package com.ingenio.notification.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.notification.bean.ResponseBodyBean;
import com.ingenio.notification.service.InitialSubjectSettingService;


@CrossOrigin
@RestController
public class InitialSubjectSettingController {

	@Autowired
	InitialSubjectSettingService initialSubjectSettingService;
	
	@PostMapping(value="/saveStandardFromGlobal")
	public @ResponseBody ResponseBodyBean<Integer> saveStandardFromGlobal(@RequestParam("globalStandatdId") Integer globalStandatdId[],
			@RequestParam("userId") Integer userId,@RequestParam("schoolId") Integer schoolId)   {
		try {
			Integer saveId=initialSubjectSettingService.saveStandardFromGlobal(globalStandatdId,userId,schoolId);
			if(saveId !=null) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	} 
	
	@PostMapping(value="/saveSubjectFromGlobal")
	public @ResponseBody ResponseBodyBean<Integer> saveSubjectFromGlobal(@RequestParam("userId") Integer userId,@RequestParam("schoolId") Integer schoolId)   {
		try {
			Integer saveId=initialSubjectSettingService.saveSubjectFromGlobal(userId,schoolId);
			if(saveId !=null) {
				return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
			}
			else {
				return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
}
