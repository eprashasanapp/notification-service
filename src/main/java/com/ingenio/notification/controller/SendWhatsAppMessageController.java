package com.ingenio.notification.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenio.notification.bean.NotificationBean;
import com.ingenio.notification.bean.ResponseBodyBean;
import com.ingenio.notification.bean.WhatsAppMessageBean;
import com.ingenio.notification.service.SendWhatsAppMessageService;



@CrossOrigin
@RestController
public class SendWhatsAppMessageController {
	
	@Autowired
	SendWhatsAppMessageService sendWhatsAppMessageService;
	
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	
	@PreDestroy
	public void shutdonw() {
		executor.shutdown(); 
	}
		
	@PostMapping(value = "/sendWhatsAppMessageNotification",consumes = "application/json")
	@ResponseBody
	public ResponseBodyBean<String> sendWhatsAppMessage(@RequestBody WhatsAppMessageBean  whatsAppMessageBean,@RequestParam(value = "isSendWhatsAppMsg", required = false, defaultValue = "0") Integer isSendWhatsAppMsg,
			@RequestParam(value = "isSendSMS", required = false, defaultValue = "0") Integer isSendSMS,@RequestParam(value = "isSendNotification", required = false, defaultValue = "0") Integer isSendNotification,
			@RequestParam(value = "isBroadcast", required = false, defaultValue = "0") Integer isBroadcast) {
		try {
			int success=1;
			if(isSendWhatsAppMsg == 1 && isSendSMS ==0 && isSendNotification==0) {
				executor.submit(() -> sendWhatsAppMessageService.sendWhatsAppSMS(whatsAppMessageBean));
			}else if(isSendSMS == 1 && isSendWhatsAppMsg==0 && isSendNotification==0) {
				executor.submit(() -> sendWhatsAppMessageService.sendSMS(whatsAppMessageBean));
			}else if(isSendNotification == 1 && isSendWhatsAppMsg==0 && isSendSMS==0) {
				executor.submit(() -> sendWhatsAppMessageService.sendNotification(whatsAppMessageBean,isBroadcast));
			}else if(isSendWhatsAppMsg == 1 && isSendSMS ==1 && isSendNotification==0) {
				executor.submit(() -> sendWhatsAppMessageService.sendWhatsAppSMS(whatsAppMessageBean));
				executor.submit(() -> sendWhatsAppMessageService.sendSMS(whatsAppMessageBean));
			}else if(isSendWhatsAppMsg == 1 && isSendSMS ==1 && isSendNotification==1) {
				executor.submit(() -> sendWhatsAppMessageService.sendWhatsAppSMS(whatsAppMessageBean));
				executor.submit(() -> sendWhatsAppMessageService.sendSMS(whatsAppMessageBean));
				executor.submit(() -> sendWhatsAppMessageService.sendNotification(whatsAppMessageBean,isBroadcast));
			}else if(isSendWhatsAppMsg == 1 && isSendSMS ==0 && isSendNotification==1) {
				executor.submit(() -> sendWhatsAppMessageService.sendWhatsAppSMS(whatsAppMessageBean));
				executor.submit(() -> sendWhatsAppMessageService.sendNotification(whatsAppMessageBean,isBroadcast));
			}else if(isSendWhatsAppMsg == 0 && isSendSMS ==1 && isSendNotification==1) {
				executor.submit(() -> sendWhatsAppMessageService.sendSMS(whatsAppMessageBean));
				executor.submit(() -> sendWhatsAppMessageService.sendNotification(whatsAppMessageBean,isBroadcast));
			}
			if (Integer.toString(success) == "1") {
				return new ResponseBodyBean<String>("200", HttpStatus.OK, Integer.toString(success));
			} else {
				return new ResponseBodyBean<String>("404", HttpStatus.NOT_FOUND, Integer.toString(success));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<String>("500", HttpStatus.INTERNAL_SERVER_ERROR, "0");
		}
	}
	
	
	@GetMapping(value="/getAllNotificationsNew")
	ResponseBodyBean<NotificationBean> getAllNotifications(@RequestParam("renewId") Integer renewId,
			@RequestParam("role") String role,@RequestParam(value="offset",required = false,defaultValue = "0") Integer offset) {
		try {
			List<NotificationBean> notificationList=sendWhatsAppMessageService.getAllNotifications(renewId,role,offset);
			if(CollectionUtils.isNotEmpty(notificationList)) {
				return new ResponseBodyBean<NotificationBean>("200",HttpStatus.OK,notificationList);
			}
			else {
				return new ResponseBodyBean<NotificationBean>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<NotificationBean>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
	
	@GetMapping(value="/getAllNotificationsUnseenCount")
	ResponseBodyBean<Integer> getAllNotificationsUnseenCount(@RequestParam("renewId") Integer renewId,
			@RequestParam("role") String role) {
		try {
			Integer notificationCount=sendWhatsAppMessageService.getAllNotificationsUnseenCount(renewId,role);
			return new ResponseBodyBean<Integer>("200",HttpStatus.OK,notificationCount);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,0);
		}
	}
	
	@PostMapping(value="/updateNotificationSeenStatus")
	public @ResponseBody ResponseBodyBean<Integer> updateNotificationSeenStatus(@RequestParam(value="notificationId") String notificationId){
		try {
				Integer saveId = sendWhatsAppMessageService.updateNotificationSeenStatus(notificationId);
				if(saveId>0) {
					return new ResponseBodyBean<Integer>("200",HttpStatus.OK,saveId);
				}
				else {
					return new ResponseBodyBean<Integer>("404",HttpStatus.NOT_FOUND,new ArrayList<>());
				}			
		
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseBodyBean<Integer>("500",HttpStatus.INTERNAL_SERVER_ERROR,new ArrayList<>());
		}
	}
}
