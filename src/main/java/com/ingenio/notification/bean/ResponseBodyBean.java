package com.ingenio.notification.bean;

import java.util.List;

import org.springframework.http.HttpStatus;

public class ResponseBodyBean<T> {
	
	private String statusCode;
	private HttpStatus stausMessage;
	private List<T> responseList;
	private T responseData;
	private String statusMessage1;
	private String message;
	
	public ResponseBodyBean() {
		super();
	}

	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, List<T> responseList) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseList = responseList;
	}
	
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, T responseData) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseData = responseData;
	}
	
	public ResponseBodyBean(String statusCode, String stausMessage1, T responseData) {
		super();
		this.statusCode = statusCode;
		this.statusMessage1 = stausMessage1;
		this.responseData = responseData;
	}
	
	
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, T responseData,String message) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseData = responseData;
		this.message=message;
	}
	
	
	
	public ResponseBodyBean(String statusCode, HttpStatus stausMessage, List<T> responseList,String message) {
		super();
		this.statusCode = statusCode;
		this.stausMessage = stausMessage;
		this.responseList = responseList;
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getResponseList() {
		return responseList;
	}
	
	public void setResponseList(List<T> responseList) {
		this.responseList = responseList;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public HttpStatus getStausMessage() {
		return stausMessage;
	}

	public void setStausMessage(HttpStatus stausMessage) {
		this.stausMessage = stausMessage;
	}

	public T getResponseData() {
		return responseData;
	}

	public void setResponseData(T responseData) {
		this.responseData = responseData;
	}

	public String getStatusMessage1() {
		return statusMessage1;
	}

	public void setStatusMessage1(String statusMessage1) {
		this.statusMessage1 = statusMessage1;
	}
}
