package com.ingenio.notification.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

public class NotificationBean {

	private String notificationType;
	private String title;
	private String message;
	private Date notificationDate;
	private String notificationTime;
	private String notificationhour;
	private String dateDiffrenceValue;
	private Integer tabId;
	private Integer schoolId;
	private Integer renewStaffId;
	private String typeFlag;
	private String messageType;
	private String date;
	private String mobileNo;
	private Integer notificationId;
	private Integer seenUnseenStatusFlag;
	private String successFailureFlag;
	
	public String getDateDiffrenceValue() {
		return dateDiffrenceValue;
	}
	public void setDateDiffrenceValue(String dateDiffrenceValue) {
		this.dateDiffrenceValue = dateDiffrenceValue;
	}
	public String getNotificationhour() {
		return notificationhour;
	}
	public void setNotificationhour(String notificationhour) {
		this.notificationhour = notificationhour;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getNotificationTime() {
		return notificationTime;
	}
	public void setNotificationTime(String notificationTime) {
		this.notificationTime = notificationTime;
	}
	public Integer getTabId() {
		return tabId;
	}
	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public Integer getRenewStaffId() {
		return renewStaffId;
	}
	public void setRenewStaffId(Integer renewStaffId) {
		this.renewStaffId = renewStaffId;
	}
	public String getTypeFlag() {
		return typeFlag;
	}
	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Integer getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}
	public Integer getSeenUnseenStatusFlag() {
		return seenUnseenStatusFlag;
	}
	public void setSeenUnseenStatusFlag(Integer seenUnseenStatusFlag) {
		this.seenUnseenStatusFlag = seenUnseenStatusFlag;
	}
	public String getSuccessFailureFlag() {
		return successFailureFlag;
	}
	public void setSuccessFailureFlag(String successFailureFlag) {
		this.successFailureFlag = successFailureFlag;
	}
	
	

	
	
}
