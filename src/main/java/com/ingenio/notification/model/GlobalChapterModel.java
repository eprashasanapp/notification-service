package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="global_chapter")
public class GlobalChapterModel {

	private Integer globalChapterId;
	private String chapterName;
	private String chapterNo;
	private GlobalStandardMasterModel globalStandardMasterModel;
	private GlobalSubjectMasterModel globalSubjectMasterModel;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="globalChapterId", unique = true, nullable = false)
	public Integer getGlobalChapterId() {
		return globalChapterId;
	}
	public void setGlobalChapterId(Integer globalChapterId) {
		this.globalChapterId = globalChapterId;
	}
	
	@Column(name="chapter_name")
	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}
	
	@Column(name="chapterNo")
	public String getChapterNo() {
		return chapterNo;
	}
	public void setChapterNo(String chapterNo) {
		this.chapterNo = chapterNo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="globalStandardId")
	public GlobalStandardMasterModel getGlobalStandardMasterModel() {
		return globalStandardMasterModel;
	}
	public void setGlobalStandardMasterModel(GlobalStandardMasterModel globalStandardMasterModel) {
		this.globalStandardMasterModel = globalStandardMasterModel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="globalSubjectId")
	public GlobalSubjectMasterModel getGlobalSubjectMasterModel() {
		return globalSubjectMasterModel;
	}
	public void setGlobalSubjectMasterModel(GlobalSubjectMasterModel globalSubjectMasterModel) {
		this.globalSubjectMasterModel = globalSubjectMasterModel;
	}
	@Column(name="cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	
}
