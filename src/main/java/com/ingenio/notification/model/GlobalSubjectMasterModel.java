package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="global_subject_master")
public class GlobalSubjectMasterModel {

	private Integer globalSubjectId;
	private String subjectName;
	private SchoolBoardModel schoolBoardModel;
	private GlobalStandardMasterModel globalStandardMasterModel;
	private Integer subjectType;
	private String language;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "global_subjectId", unique = true, nullable = false)
	public Integer getGlobalSubjectId() {
		return globalSubjectId;
	}
	public void setGlobalSubjectId(Integer globalSubjectId) {
		this.globalSubjectId = globalSubjectId;
	}
	
	@Column(name="subjectName")
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="schoolBoardId")
	public SchoolBoardModel getSchoolBoardModel() {
		return schoolBoardModel;
	}
	public void setSchoolBoardModel(SchoolBoardModel schoolBoardModel) {
		this.schoolBoardModel = schoolBoardModel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="globalStandardId")
	public GlobalStandardMasterModel getGlobalStandardMasterModel() {
		return globalStandardMasterModel;
	}
	public void setGlobalStandardMasterModel(GlobalStandardMasterModel globalStandardMasterModel) {
		this.globalStandardMasterModel = globalStandardMasterModel;
	}
	
	@Column(name="subjectType")
	public Integer getSubjectType() {
		return subjectType;
	}
	public void setSubjectType(Integer subjectType) {
		this.subjectType = subjectType;
	}
	
	@Column(name="language")
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Column(name="cDate")
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	
	
}
