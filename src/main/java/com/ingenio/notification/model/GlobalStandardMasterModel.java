package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="global_standard_master")
public class GlobalStandardMasterModel {

	private Integer globalStandardId;
	private String standardName;
	private String ageRange;
	private Integer standardPriority;
	private Integer isDel;
	private Integer isEdit;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate=new java.sql.Date(new java.util.Date().getTime());
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "global_stdId", unique = true, nullable = false)
	public Integer getGlobalStandardId() {
		return globalStandardId;
	}
	public void setGlobalStandardId(Integer globalStandardId) {
		this.globalStandardId = globalStandardId;
	}
	
	@Column(name="standardName")
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	@Column(name="ageRange")
	public String getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	
	@Column(name="standardPriority")
	public Integer getStandardPriority() {
		return standardPriority;
	}
	public void setStandardPriority(Integer standardPriority) {
		this.standardPriority = standardPriority;
	}
	
	@Column(name="isDel")
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	
	@Column(name="isEdit")
	public Integer getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "cDate", updatable= false)
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	
}
