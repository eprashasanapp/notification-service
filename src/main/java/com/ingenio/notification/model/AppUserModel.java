package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "app_user")

public class AppUserModel{

	private Integer appUserRoleId;
	
	private String username;
	
	private String password;
	
	private String firstName;
	
	private String lastName;
	
	private String question1;
	
	private String answer1;
	
	private String question2;
	
	private String answer2;
	
	private String settingString;
	
	private String subAdmin;
	
	private String activatedby;
	
	private String remark;
	
	private Integer staffId;
	
	private Integer schoolid;
	
	private String roleName;
	
	private AppUserRoleModel userId;
	
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	private String isDel = "0";
	
	private Date delDate;
	
	private AppUserRoleModel deleteBy;
	
	private String isEdit = "0";
	
	private Date editDate;
	
	private AppUserRoleModel editBy;

	private String deviceType = "0";
	
	private String ipAddress;
	
	private String macAddress;
	
	private String sinkingFlag = "0";
	
	private String activeFlag = "0";
	
	private String IMEICode = "0";
	
	@Column(name = "USERNAME", nullable = false)
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "PASSWORD", nullable = false)
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "FIRST_NAME", nullable = false)
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name = "LAST_NAME", nullable = false)
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "Question1", length = 500)
	public String getQuestion1() {
		return question1;
	}
	
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	
	@Column(name = "Answer1", length = 500)
	public String getAnswer1() {
		return answer1;
	}
	
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}
	
	@Column(name = "Question2", length = 500)
	public String getQuestion2() {
		return question2;
	}
	
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	
	@Column(name = "Answer2", length = 500)
	public String getAnswer2() {
		return answer2;
	}
	
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}
	
	@Column(name = "settingString", length = 50)
	public String getSettingString() {
		return settingString;
	}
	
	public void setSettingString(String settingString) {
		this.settingString = settingString;
	}
	
	@Column(name = "subAdmin", length = 50)
	public String getSubAdmin() {
		return subAdmin;
	}
	
	public void setSubAdmin(String subAdmin) {
		this.subAdmin = subAdmin;
	}
	
	@Column(name = "Activatedby", length = 500)
	public String getActivatedby() {
		return activatedby;
	}
	
	public void setActivatedby(String activatedby) {
		this.activatedby = activatedby;
	}
	
	@Column(name = "Remark", length = 500)
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "staffID", length = 500)
	public Integer getStaffId() {
		return staffId;
	}
	
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	@Column(name = "schoolid")
	public Integer getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(Integer schoolid) {
		this.schoolid = schoolid;
	}

	@Column(name = "userId")
	public AppUserRoleModel getUserId() {
		return userId;
	}
	
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	/*@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)*/
	@Column(name = "cDate", updatable = false)
	public Date getcDate() {
		return cDate;
	}
	
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name = "isDel", columnDefinition = "default '0'")
	public String getIsDel() {
		return isDel;
	}
	
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name = "delDate")
	public Date getDelDate() {
		return delDate;
	}
	
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	
	@Column(name = "deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@Column(name = "isEdit", columnDefinition = "default '0'")
	public String getIsEdit() {
		return isEdit;
	}
	
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name = "editDate")
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	@Column(name = "editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	
	@Column(name = "deviceType", columnDefinition = "default '0'")
	public String getDeviceType() {
		return deviceType;
	}
	
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	@Column(name = "ipAddress", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	@Column(name = "macAddress", length = 50)
	public String getMacAddress() {
		return macAddress;
	}
	
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	@Column(name = "sinkingFlag", columnDefinition = "default '0'")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Id
	@Column(name = "APPUSERROLEID", nullable = false)
	public Integer getAppUserRoleId() {
		return appUserRoleId;
	}

	public void setAppUserRoleId(Integer appUserRoleId) {
		this.appUserRoleId = appUserRoleId;
	}

	@Column(name = "activeFlag", columnDefinition = "default '0'")
	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Column(name = "RoleName", length = 50)
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Column(name = "IMEICode", columnDefinition = "default '0'")
	public String getIMEICode() {
		return IMEICode;
	}

	public void setIMEICode(String iMEICode) {
		IMEICode = iMEICode;
	}
	
}