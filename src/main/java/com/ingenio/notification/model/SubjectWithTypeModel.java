package com.ingenio.notification.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="subject_with_type")
public class SubjectWithTypeModel {

	private Integer subjectWithTypeId;
	private SubjectModel subjectModel;
	private Integer subjectTypeId;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="subject_with_type_Id" )
	public Integer getSubjectWithTypeId() {
		return subjectWithTypeId;
	}
	public void setSubjectWithTypeId(Integer subjectWithTypeId) {
		this.subjectWithTypeId = subjectWithTypeId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subject_id")
	public SubjectModel getSubjectModel() {
		return subjectModel;
	}
	public void setSubjectModel(SubjectModel subjectModel) {
		this.subjectModel = subjectModel;
	}
	
	@Column(name="subject_type_id")
	public Integer getSubjectTypeId() {
		return subjectTypeId;
	}
	public void setSubjectTypeId(Integer subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}
	
	
}
