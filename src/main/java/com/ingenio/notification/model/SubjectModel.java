package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "subjects")
public class SubjectModel {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	@Column(name = "subject_id")
	private int subjectId;
	
	@ManyToOne
	@JoinColumn(name = "year")
	private YearMasterModel yearMasterModel;
	
	@ManyToOne
	@JoinColumn(name = "standard")
	private StandardMasterModel standardMasterModel;
	
	@Column(name = "subject_name")
	private String subjectName;
	
	@Column(name = "subject_nameGL")
	private String subjectNameGL;
	
	@Column(name = "is_eligible_Grade")
	private Integer isEligibleGrade;
	
	@Column(name = "is_eligible_grace")
	private Integer isEligibleGrace;
	
	@Column(name = "grade_type",nullable = true )
	private String gradeType;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "updated_date")
	private String updatedDate;
	
	@Column(name = "status")
	private int status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_cat_id")
	private SubjectCategoryModel subjectCategoryModel;

	@Column(name="practicaltheoryFlag")
	private String practicaltheoryFlag;
	
	@Column(name = "priority_for_marksheet")
	private int priorityForMarksheet;
	
	@Column(name = "credit")
	private String credit;
	
	@Column(name = "course_code")
	private String courseCode;
	
	@Column(name = "yearsemesterflag")
	private String yearsemesterflag;
	
	@Column(name = "compulElectiveFlag")
	private String compulElectiveFlag;
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	@Column(name = "cDate", updatable = false)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	
	@Column(name = "isDel")
	private String isDel = "0";
	
	@Column(name = "isEdit")
	private String isEdit = "0";
	
	@Column(name = "sinkingFlag")
	private String sinkingFlag = "0";
	
	@Column(name = "delDate")
	private Date delDate;
	
	@Column(name = "editDate")
	private Date editDate;
	
	@Column(name = "deviceType")
	private String deviceType = "0";
	
	@Column(name = "ipAddress")
	private String ipAddress;
	
	@Column(name = "macAddress")
	private String macAddress;
	
	@Column(name = "credit_w_attendance")
	private String creditWithoutAttendance;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolid", nullable = false)
	private SchoolMasterModel schoolMasterModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userID", updatable = false)
	private AppUserRoleModel userId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deleteBy")
	private AppUserRoleModel deleteBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editBy")
	private AppUserRoleModel editBy;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupId")
	private ExamGroupModel examGroupModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subGroupId")
	private ExamSubGroupModel examSubGroupModel;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "semesterId")
	private SemesterMasterModel semesterMasterModel;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="global_subjectId")
	private GlobalSubjectMasterModel globalSubjectMasterModel;

	@Column(name="rc_amount")
	private Integer rcAmount;
	
	@Column(name="rwc_amount")
	private Integer rwcAmount;
	
	@Column(name="subjectCourseFlag")
	private String subjectCourseFlag;
	
	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectNameGL() {
		return subjectNameGL;
	}

	public void setSubjectNameGL(String subjectNameGL) {
		this.subjectNameGL = subjectNameGL;
	}

	public Integer getIsEligibleGrade() {
		return isEligibleGrade;
	}

	public void setIsEligibleGrade(Integer isEligibleGrade) {
		this.isEligibleGrade = isEligibleGrade;
	}

	public Integer getIsEligibleGrace() {
		return isEligibleGrace;
	}

	public void setIsEligibleGrace(Integer isEligibleGrace) {
		this.isEligibleGrace = isEligibleGrace;
	}

	public String getGradeType() {
		return gradeType;
	}

	public void setGradeType(String gradeType) {
		this.gradeType = gradeType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPriorityForMarksheet() {
		return priorityForMarksheet;
	}

	public void setPriorityForMarksheet(int priorityForMarksheet) {
		this.priorityForMarksheet = priorityForMarksheet;
	}

	public Date getcDate() {
		return cDate;
	}

	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public String getSinkingFlag() {
		return sinkingFlag;
	}

	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}

	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}

	public AppUserRoleModel getUserId() {
		return userId;
	}

	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}

	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}

	public AppUserRoleModel getEditBy() {
		return editBy;
	}

	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}

	public YearMasterModel getYearMasterModel() {
		return yearMasterModel;
	}

	public void setYearMasterModel(YearMasterModel yearMasterModel) {
		this.yearMasterModel = yearMasterModel;
	}

	public StandardMasterModel getStandardMasterModel() {
		return standardMasterModel;
	}

	public void setStandardMasterModel(StandardMasterModel standardMasterModel) {
		this.standardMasterModel = standardMasterModel;
	}

	public String getCompulElectiveFlag() {
		return compulElectiveFlag;
	}

	public void setCompulElectiveFlag(String compulElectiveFlag) {
		this.compulElectiveFlag = compulElectiveFlag;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public ExamGroupModel getExamGroupModel() {
		return examGroupModel;
	}

	public void setExamGroupModel(ExamGroupModel examGroupModel) {
		this.examGroupModel = examGroupModel;
	}

	public ExamSubGroupModel getExamSubGroupModel() {
		return examSubGroupModel;
	}

	public void setExamSubGroupModel(ExamSubGroupModel examSubGroupModel) {
		this.examSubGroupModel = examSubGroupModel;
	}

	public String getPracticaltheoryFlag() {
		return practicaltheoryFlag;
	}

	public void setPracticaltheoryFlag(String practicaltheoryFlag) {
		this.practicaltheoryFlag = practicaltheoryFlag;
	}

	public SubjectCategoryModel getSubjectCategoryModel() {
		return subjectCategoryModel;
	}

	public void setSubjectCategoryModel(SubjectCategoryModel subjectCategoryModel) {
		this.subjectCategoryModel = subjectCategoryModel;
	}

	public String getYearsemesterflag() {
		return yearsemesterflag;
	}

	public void setYearsemesterflag(String yearsemesterflag) {
		this.yearsemesterflag = yearsemesterflag;
	}

	public SemesterMasterModel getSemesterMasterModel() {
		return semesterMasterModel;
	}

	public void setSemesterMasterModel(SemesterMasterModel semesterMasterModel) {
		this.semesterMasterModel = semesterMasterModel;
	}

	public String getCreditWithoutAttendance() {
		return creditWithoutAttendance;
	}

	public void setCreditWithoutAttendance(String creditWithoutAttendance) {
		this.creditWithoutAttendance = creditWithoutAttendance;
	}

	public GlobalSubjectMasterModel getGlobalSubjectMasterModel() {
		return globalSubjectMasterModel;
	}

	public void setGlobalSubjectMasterModel(GlobalSubjectMasterModel globalSubjectMasterModel) {
		this.globalSubjectMasterModel = globalSubjectMasterModel;
	}

	public Integer getRcAmount() {
		return rcAmount;
	}

	public void setRcAmount(Integer rcAmount) {
		this.rcAmount = rcAmount;
	}

	public Integer getRwcAmount() {
		return rwcAmount;
	}

	public void setRwcAmount(Integer rwcAmount) {
		this.rwcAmount = rwcAmount;
	}

	public String getSubjectCourseFlag() {
		return subjectCourseFlag;
	}

	public void setSubjectCourseFlag(String subjectCourseFlag) {
		this.subjectCourseFlag = subjectCourseFlag;
	}

	
	
	
}