package com.ingenio.notification.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "school_board")
public class SchoolBoardModel {
	
	private int schoolBoardId;
	private String schoolBoard;
	private SchoolTypeModel schoolTypeModel;
	private String sinkingFlag = "0";
	private Integer schoolId;
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	public int getSchoolBoardId() {
		return schoolBoardId;
	}
	public void setSchoolBoardId(int schoolBoardId) {
		this.schoolBoardId = schoolBoardId;
	}
	
	@Column(name="schoolBoard")
	public String getSchoolBoard() {
		return schoolBoard;
	}
	public void setSchoolBoard(String schoolBoard) {
		this.schoolBoard = schoolBoard;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolTypeId")
	public SchoolTypeModel getSchoolTypeModel() {
		return schoolTypeModel;
	}
	public void setSchoolTypeModel(SchoolTypeModel schoolTypeModel) {
		this.schoolTypeModel = schoolTypeModel;
	}
	@Column(name = "sinkingFlag", nullable = false)
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	
	@Column(name="schoolId")
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	
	
	

}
