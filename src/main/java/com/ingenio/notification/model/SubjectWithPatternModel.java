package com.ingenio.notification.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="subject_with_pattern")
public class SubjectWithPatternModel {
	private Integer subjectPatternId;
	private SchoolMasterModel schoolMasterModel;
	private SubjectModel subjectModel;
	private SubjectPatternModel subjectPatternModel;
	private AppUserRoleModel userId;
	private AppUserRoleModel deleteBy;
	private AppUserRoleModel editBy;
	@Generated(GenerationTime.ALWAYS)
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date cDate = new java.sql.Date(new java.util.Date().getTime());
	private String isDel = "0";
	private String isEdit = "0";
	private String sinkingFlag = "0";
	private Date delDate;
	private Date editDate;
	private String deviceType = "0";
	private String ipAddress;
	private String macAddress;
	
	@Id
	@Column(name="subject_pattern_id")
	public Integer getSubjectPatternId() {
		return subjectPatternId;
	}
	public void setSubjectPatternId(Integer subjectPatternId) {
		this.subjectPatternId = subjectPatternId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="schoolid")
	public SchoolMasterModel getSchoolMasterModel() {
		return schoolMasterModel;
	}
	public void setSchoolMasterModel(SchoolMasterModel schoolMasterModel) {
		this.schoolMasterModel = schoolMasterModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="subject_id")
	public SubjectModel getSubjectModel() {
		return subjectModel;
	}
	public void setSubjectModel(SubjectModel subjectModel) {
		this.subjectModel = subjectModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pattern_id")
	public SubjectPatternModel getSubjectPatternModel() {
		return subjectPatternModel;
	}
	public void setSubjectPatternModel(SubjectPatternModel subjectPatternModel) {
		this.subjectPatternModel = subjectPatternModel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="userId")
	public AppUserRoleModel getUserId() {
		return userId;
	}
	public void setUserId(AppUserRoleModel userId) {
		this.userId = userId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="deleteBy")
	public AppUserRoleModel getDeleteBy() {
		return deleteBy;
	}
	public void setDeleteBy(AppUserRoleModel deleteBy) {
		this.deleteBy = deleteBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="editBy")
	public AppUserRoleModel getEditBy() {
		return editBy;
	}
	public void setEditBy(AppUserRoleModel editBy) {
		this.editBy = editBy;
	}
	
	@Column(name="cDate")
	public Date getcDate() {
		return cDate;
	}
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	
	@Column(name="isDel")
	public String getIsDel() {
		return isDel;
	}
	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}
	
	@Column(name="isEdit")
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	
	@Column(name="sinkingFlag")
	public String getSinkingFlag() {
		return sinkingFlag;
	}
	public void setSinkingFlag(String sinkingFlag) {
		this.sinkingFlag = sinkingFlag;
	}
	@Column(name="delDate")
	public Date getDelDate() {
		return delDate;
	}
	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}
	@Column(name="editDate")
	public Date getEditDate() {
		return editDate;
	}
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	@Column(name="deviceType")
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	@Column(name="ipAddress")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	@Column(name="macAddress")
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	

}
