package com.ingenio.notification.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taluka_tab")
public class TalukaModel {
	private static final long serialVersionUID = 1L;
		private Integer talukaId;
	private String taluka;
	private DistrictModel districtModel;
	private StateModel stateModel;

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "talukaId", unique = true, nullable = false)
	public Integer getTalukaId() {
		return talukaId;
	}
	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}
	public String getTaluka() {
		return taluka;
	}
	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "districtId",nullable = false)
	public DistrictModel getDistrictModel() {
		return districtModel;
	}
	public void setDistrictModel(DistrictModel districtModel) {
		this.districtModel = districtModel;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stateId",nullable = false)
	public StateModel getStateModel() {
		return stateModel;
	}
	public void setStateModel(StateModel stateModel) {
		this.stateModel = stateModel;
	}
	@Override
	public String toString() {
		return "TalukaModel [talukaId=" + talukaId + ", taluka=" + taluka + ", districtModel=" + districtModel
				+ ", stateModel=" + stateModel + "]";
	}
	
	
	
	
	
}
